﻿using System;
namespace cc.gufei.mine
{
	public struct Block
	{
		/// <summary>
		/// 区块位置
		/// </summary>
		public int Index { get; set; }
		/// <summary>
		/// 区块生成时间戳
		/// </summary>
		public string TimeStamp { get; set; }
		/// <summary>
		/// 心率数值
		/// </summary>
		public int BPM { get; set; }
		/// <summary>
		/// 区块 SHA-256 散列值
		/// </summary>
		public string Hash { get; set; }
		/// <summary>
		/// 前一个区块 SHA-256 散列值
		/// </summary>
		public string PrevHash { get; set; }
		/// <summary>
		/// 下一个区块生成难度
		/// </summary>
		public int Difficulty { get; set; }
		/// <summary>
		/// 随机值
		/// </summary>
		public string Nonce { get; set; }
	}
}
